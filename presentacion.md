# Universidad Nacional Autónoma de México
# Facultad de Ciencias
# Ciencias de la Computación

## Redes de Computadoras

Semestre 2018-2

+ **José Luis Torres Rodríguez**
+ Andrés Leonardo Hernández Bermúdez
+ Fernando Castañeda González

### Requisitos

+ Conocimientos básicos de GNU/Linux
+ Conocimientos de Sistemas Operativos
+ Conocimientos de programación en un lenguaje estructurado u orientado a objetos
+ No tener materias encimadas
+ Asistencia regular a clase

### Objetivos

+ Conocer y comprender los conceptos, técnicas y algoritmos más importantes involucrados en redes de computadoras
+ Conocer los modelos de referencia empleados para la especificación de redes y hacer una revisión de TCP/IP
+ Estudiar las caracterísitcas de las redes más conocidas y usadas actualmente
+ Creación de una red y aplicaciones para redes
+ Implementación de diversos servicios de red
+ Tener un panorama de las posibilidades, implicaciones, ventajas y riesgos del uso de una red
+ Abordar temás básicos de seguridad en redes a nivel práctico
+ Obtener conocimientos de administración de sistemas GNU/Linux enfocados a redes, mediante la revisión de comandos y aplicaciones propios del sistema operativo

### Sistemas Operativos

+ GNU/Linux
+ Windows

### Lenguaje de programación

+ C

### Impartición de las clases

#### Teoría

+ Lunes y miércoles de 18:30 a 20:00 horas en el [salón 302 de Yelizcalli][salon-Y302]
+ Martes y jueves de 19:00 a 20:00 horas en el [salón 302 de Yelizcalli][salon-Y302]

#### Laboratorio

+ Lunes de 14:00 a 16:00 horas
+ [Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información][Taller-Tlahuizcalpan]

### Contenido del curso

### Contenido del curso

+ Conceptos generales de redes de computadoras
+ Repaso de GNU/Linux
+ Elementos de programación en lenguaje C
+ Sockets de Berkeley
+ Modelos de referencia para especificación de redes de computadoras
  * El modelo de referencia OSI
  * El modelo de referencia TCP/IP
  * Comparación de ambos modelos
+ Revisión de las capas del modelo de referencia TCP/IP
  * Capa de Aplicación
     - Revisión de protocolos de capa de aplicación
     - Creación de aplicaciones para implementar protocolos de capa de aplicación
  * Capa de Transporte
     - Revisión de TCP y UDP
     - Protocolos de transferencia confiable
     - Control de flujo
     - Control de congestión
     - Capturas de tráfico, revisión de cabeceras sobre paquetes capturados
     - Creación de aplicaciones para el manejo de paquetes de capa de transporte
  * Capa de Red
     - Revisión de IP: IPv4, IPv6
     - Revisión de ICMP
     - Protocolos de ruteo
         + Vector de distancia
         + Estado del enlace
     - Funcionamiento de un switch y un router
     - RIP
     - OSPF
     - BGP
     - Broadcast y multicast
     - iptables
     - Creación de una red con servicios básicos y conexión a Internet
  * Capa de Enlace
     - Técnicas de detección y corrección de errores
         + Bits de paridad
         + Sumas de verificación
         + CRC
     - Cabeceras de capa de enlace
     - LAN's y protocolos de acceso múltiple
     - Direccionamiento en capa de enlace
         + Direcciones MAC
         + ARP
     - Revisión de Ethernet
     - PPP
+ Revisión de las características de los principales tipos de redes empleadas actualmente
+ Redes inalámbricas
  * 802.11
     - IP móvil
+ Implementación de una red
  * Servicios de red en sistemas GNU/Linux
+ Seguridad en redes
  * Principios de criptografía (práctica)
     - SSL
     - VPN
     - IPsec
     - WAP
+ Aplicaciones en redes
  * Sistemas operativos en red
  * Servicios de red
  * Sistemas distribuidos

### Evaluación del semestre

Se considerarán los siguientes elementos para llevar a cabo la evaluación:

#### 1. Asistencia regular a clase

#### 2. Exámenes

Calendarización de examenes

| Examen	| Fecha				|
|--------------:|:------------------------------|
| Parcial 1	| Martes, 20 de febrero de 2018	|
| Parcial 2	| Jueves, 22 de marzo   de 2018	|
| Parcial 3	| Jueves, 19 de abril   de 2018	|
| Parcial 4	| Jueves, 17 de mayo    de 2018	|

##### Requisitos para presentar examen

+ Haber entregado puntualmente las tareas anteriores a la fecha del examen
+ No habrá reposiciones de examenes
+ Es requisito indispensable el contar con un promedio aprobatorio en los examenes y haberlos presentado todos, sin excepción, para tener derecho a obtener una calificación final aprobatoria en el semestre

#### 3. Prácticas

+ Prácticas sobre cada uno de los temas principales vistos a lo largo del semestre
+ Las prácticas están diseñadas en forma seriada, por lo que se sugiere no omitir la elaboración de ninguna de ellas.
+ Es requisito indispensable el haber entregado todas las prácticas para tener derecho a obtener una calificación final aprobatoria en el semestre

#### 4. Exposiciones

+ Se propondrá una lista de temas a exponer *en equipo*
+ Cada equipo contará con al menos dos semanas para la preparación del tema correspondiente

#### 5. Tareas teórico-prácticas

+ En estas tareas se aplicarán todos los conocimientos vistos a la fecha, complementados con material adicional

#### 6. Participaciones en clase

+ Éstas son **obligatorias** y son un elemento fundamental en la evaluación final
+ Se tomarán en cuenta sobre todo aquellas participaciones que refuercen lo visto en clase y que constituyan una aportación importante a la misma

### Notas

+ La entrega de todos los trabajos será improrrogable
+ Todos los trabajos duplicados _serán evaluados con cero_, sin hacer indagaciones
+ La calificación final se entregará **personalmente** a más tardar el día de la segunda vuelta de los examenes finales
+ Quien no se presente el día señalado a recibir su calificación está aceptando la evaluación que se haga de su trabajo durante el semestre
+ Sin excepción, no se aceptarán reclamaciones fuera de la fecha señalada en el punto anterior
+ Sin excepciones, no se permitirá renunciar a ninguna calificación
+ La calificación de NP sólo se asignará a quien no haya entregado ningún trabajo y no haya presentado ningún examen
+ De ninguna manera se dará un trato ni una calificación especial a ningún estudiante, por el hecho de trabajar, tener un promedio alto en su historial académico, contar con algún tipo de beca o tener materias encimadas
+ Tampoco se tomarán en cuenta recomendaciones de ninguna persona para asignar la calificación final

### Evaluación

La calificación final se calculará en base a los siguientes porcentajes:

| Elemento	|Valor	|
|--------------:|:------|
| Exposición	| 10%	|
| Examenes	| 30%	|
| Prácticas	| 40%	|
| Tareas	| 20%	|

Las participaciones que aporten ideas a la clase pueden ayudar a incrementar la calificación final

### Observaciones adicionales

+ Se prohíben los teléfonos celulares durante la clase

    * Éstos deberán apagarse o ponerse en modo vibrador y no contestarse en el salón o laboratorio
    * Quien responda llamadas dentro del salón o laboratorio deberá retirarse, por respeto a sus compañeros

+ Por ningún motivo se repetirán clases anteriores a ninguna persona, por el hecho de no haber podido asistir a las mismas, a menos que la mayoría lo solicite y justifique con argumentos diferentes al mencionado

+ Al inicio del semestre se formarán equipos de trabajo, para llevar a cabo las tareas que lo requieran, tomando en cuenta lo siguiente:

    * El número de integrantes de los equipos se establecerá en base a la cantidad de inscritos
    * Una vez formados lo equipos no se permitirá hacer cambios de los integrantes
    * El trabajo en equipo no implica la división de la tarea por partes iguales entre los integrantes del mismo
    * Se dará por sentado que estos trabajos se desarrollaron colectivamente por todos los integrantes, por lo que no se evaluará cada parte individualmente; cada uno de los miembros de un equipo deberá poder defender, en su totalidad, el trabajo entregado

<!--
+ Correo electrónico para dudas y preguntas:

    * <code><redes-alumnos@ciencias.unam.mx></code>
-->

+ Para las tareas que se entreguen en papel, se sugiere hacer uso de hojas recicladas

+ No es necesario hacer la entrega de los trabajos en fólder, sobre, etc

+ De cada trabajo entregado se les podrá solicitar – en cualquier momento – responder preguntas acerca de o adicionales al mismo.

+ Todos los trabajos escritos deberán ser entregados o enviados por correo, personalmente

+ Se prohíbe introducir alimentos y bebidas al taller asignado para las sesiones prácticas

+ Ningún alumno deberá entrar al taller asignado para las sesiones prácticas, hasta que esté presente el titular, el ayudante o el laboratorista

+ Tampoco deberán permanecer dentro después de finalizada la clase, a menos que se cuente con la autorización de la Coordinación de la Licenciatura en Ciencias de la Computación, para hacer uso de este espacio fuera del horario normal

### Bibliografía básica

>>>
+ Computer Networking: a top down approach. 6th edition  
    Kurose, James F.  
    Ross, Keith W.  
    Addison Wesley

  * [Biblioteca Digital, UNAM][kurose-bidiunam-busqueda]
  * [Catálogo LIBRUNAM][kurose-librunam-catalogo]
  * [Búsqueda LIBRUNAM][kurose-librunam-busqueda]

+ Redes de computadoras  
    Tanenbaum, Andrew S.  
    Pearson/Prentice Hall

  * [Biblioteca Digital, UNAM][tanenbaum-bidiunam-busqueda]
  * [Catálogo LIBRUNAM][tanenbaum-librunam-catalogo]
>>>

### Ligas de interés

+ <http://www.fciencias.unam.mx/asignaturas/714.pdf>
+ <http://www.fciencias.unam.mx/docencia/horarios/presentacion/290785>
+ <http://www.fciencias.unam.mx/docencia/horarios/detalles/290785>
+ <http://www.fciencias.unam.mx/licenciatura/asignaturas/218/714>
+ <http://www.fciencias.unam.mx/licenciatura/asignaturas/1556/714>
+ <http://www.fciencias.unam.mx/docencia/horarios/20182/218/714>
+ <http://www.fciencias.unam.mx/docencia/horarios/20182/1556/714>

[kurose-bidiunam-busqueda]: http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&bquery=(TI+Computer+networking+%3a+a+top-down+approach)+AND+(AU+Kurose%2c+James+F.)&cli0=FT1&clv0=Y&lang=es&type=1&site=eds-live "Biblioteca Digital, UNAM"
[kurose-librunam-catalogo]: http://librunam.dgbiblio.unam.mx:8991/F/PBPPUGKMY7X4SDXMKFD7I6QS8FI62XUMS8UMJNA5IR27JYGYR5-63897?func=short-refine-exec&set_number=035774&request_op=AND&request=Kurose%2C+James+F.&find_code=WAT&x=73&y=17&filter_code_1=WLN&filter_request_1=&filter_code_2=WYR&filter_request_2=&filter_code_3=WYR&filter_request_3= "Catálogo LIBRUNAM"
[kurose-librunam-busqueda]: http://132.248.67.82:8991/F/-/?func=direct&doc_number=001200728current_base=MX001 "Búsqueda LIBRUNAM"

[tanenbaum-bidiunam-busqueda]: http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&bquery=(TI+Computer+networks)+AND+(AU+Tanenbaum%2c+Andrew+S.)&cli0=FT1&clv0=Y&lang=es&type=1&site=eds-live "Biblioteca Digital, UNAM"
[tanenbaum-librunam-catalogo]: http://librunam.dgbiblio.unam.mx:8991/F/PBPPUGKMY7X4SDXMKFD7I6QS8FI62XUMS8UMJNA5IR27JYGYR5-10009?func=short-refine-exec&set_number=036004&request_op=AND&request=Tanenbaum%2C+Andrew+S.&find_code=WAT&x=66&y=13&filter_code_1=WLN&filter_request_1=&filter_code_2=WYR&filter_request_2=&filter_code_3=WYR&filter_request_3= "Catálogo LIBRUNAM"

[salon-Y302]: http://www.fciencias.unam.mx/plantel/horariosalon/20182/448 "Salón 302 de Yelizcalli"
[Taller-Tlahuizcalpan]: http://www.fciencias.unam.mx/plantel/horariosalon/20182/258 "Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información"

