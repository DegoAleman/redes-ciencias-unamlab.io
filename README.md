# Universidad Nacional Autónoma de México
# Facultad de Ciencias
# Ciencias de la Computación

## Redes de Computadoras

Semestre 2018-2

### Presentación del curso

+ [Presentación del curso](presentacion.md "Generalidades del curso")

### Prácticas

+ [Prácticas](http://redes-ciencias-unam.gitlab.io/practicas/ "Índice de prácticas")
  - [Archivos PDF de las prácticas](/public/practicas "Especificaciones de las prácticas en formato PDF")

### Temas

+ [Guía rápida de `git`](git.md "9418/tcp")

#### Capa de Aplicación

+ [DNS - Domain Name System](dns.md "53/udp , 53/tcp")
+ [WHOIS](whois.md "43/tcp")
+ [HTTP - HyperText Transfer Protocol](http.md "80/tcp , 443/tcp")
+ [SSL - Secure Sockets Layer](ssl.md "ssl")
+ [SMTP - Simple Mail Transfer Protocol](smtp.md "25/tcp , 465/tcp , 587/tcp")

#### Capa de transporte

+ [TCP - Transmission Control Protocol](tcp.md "TCP")

### Tareas

#### Capa de Aplicación

+ [Tarea DNS y WHOIS](tareas/tarea-dns.md "Tarea sobre los protocolos DNS y WHOIS")
+ [Tarea HTTP](tareas/tarea-http.md "Tarea sobre el protocolo HTTP")
+ [Tarea SSL](tareas/tarea-ssl.md "Tarea sobre el protoclo SSL")


### Ligas de interés

+ <http://redes-ciencias-unam.gitlab.io/>
+ <http://redes-ciencias-unam.gitlab.io/practicas/>
+ <http://tinyurl.com/ListaRedes-2018-2>
+ <http://www.fciencias.unam.mx/asignaturas/714.pdf>
+ <http://www.fciencias.unam.mx/docencia/horarios/detalles/290785>
+ <http://www.fciencias.unam.mx/licenciatura/asignaturas/1556/714>
